#!/bin/sh
echo "pre-build steps:"
echo "authenticating with AWS ECR..."
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 231772804669.dkr.ecr.us-east-1.amazonaws.com
echo "build steps:"
echo "build image...."
docker build -t  231772804669.dkr.ecr.us-east-1.amazonaws.com/react-docker-aws:latest .
echo "push image:"
docker push 231772804669.dkr.ecr.us-east-1.amazonaws.com/react-docker-aws:latest
echo "updating AWS ECS Service"
aws ecs update-service --cluster awscluster --service service_aws --force-new-deployment
echo "Done!"

